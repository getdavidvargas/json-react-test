import React from "react";
import ReactDOM from "react-dom";
import * as env from "../package.json";

import "./index.css";

const App = () => (
  <div className="container">
    <div>Name: json-test</div>
    <div>Framework: react</div>
    <div>Language: TypeScript</div>
    <div>CSS: Empty CSS</div>
    <div>{env.name}</div>
  </div>
);
ReactDOM.render(<App />, document.getElementById("app"));
